// The `reduce` function takes an array `elements`, a callback function `cb`, and an optional starting value `startingValue`
function reduce(elements, cb, startingValue) {
  // Check if the array is empty
  if (elements.length === 0) {
    // Log a message if the array is empty and return the starting value
    console.log("Array is empty");
    return startingValue;
  } else {
    // If starting value is undefined, use the first element of the array as the initial value
    if (startingValue === undefined) {
      startingValue = elements[0];
    }

    // Iterate over the elements array using a for loop
    for (let index = 0; index < elements.length; index++) {
      // Call the provided callback function with the current accumulator value and the current array element
      startingValue = cb(startingValue, elements[index]);
    }

    // Return the final accumulated value
    return startingValue;
  }
}

// Export the 'reduce' function to make it available for use in other files
module.exports = reduce;
