// The `flatten` function takes a nested array `elements`
function flatten(elements) {
  // Initialize an empty array to store the flattened elements
  let result = [];

  // A recursive function to flatten the nested array
  function iterator(elements) {
    // Iterate over the elements array using a for loop
    for (let index = 0; index < elements.length; index++) {
      // Check if the current element is an array
      if (Array.isArray(elements[index])) {
        // If it's an array, recursively call the iterator function
        iterator(elements[index]);
        // Continue to the next iteration without processing the current element
        continue;
      }

      // If the current element is not an array, push it to the result array
      result.push(elements[index]);
    }
  }

  // Start the recursion with the initial nested array
  iterator(elements);

  // Return the flattened result array
  return result;
}

// Export the 'flatten' function to make it available for use in other files
module.exports = flatten;
