// Importing the 'find' function from the '../find.js' file
const find = require("../find.js");

// An array of items to be searched
const items = [1, 2, 3, 4, 5, 5];

// Using the 'find' function to check if any element is greater than 4
let result = find(items, (value) => {
  // Callback function: Returns true if the current value is greater than 4
  return value > 4;
});

// Logging the result of the search operation to the console
console.log(result);
