// Importing the 'each' function from the '../each.js' module
let each = require("../each.js");

// An array of items to iterate over
const items = [1, 2, 3, 4, 5, 5];

// Using the 'each' function to iterate over the items array
// The 'each' function takes a callback function as its second argument
each(items, (value, index) => {
  // Inside the callback function, log the index and value of each element
  console.log(index + "->" + value);
});
