// Importing the 'filter' function from the '../filter.js' file
const filter = require("../filter.js");

// An array of items to be filtered
const items = [1, 2, 3, 4, 5, 5];

// Using the 'filter' function to filter elements greater than 3
let result = filter(items, (value) => {
  // Callback function: Returns true if the current value is greater than 3
  return value > 3;
});

// Logging the result of the filtering operation to the console
console.log(result);
