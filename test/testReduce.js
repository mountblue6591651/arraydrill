// Importing the 'reduce' function from the '../reduce.js' file
let reduce = require("../reduce.js");

// An array of items to be reduced
const items = [1, 2, 3, 4, 5, 5];

// Using the 'reduce' function to accumulate the sum of the elements in the 'items' array
let result = reduce(
  items,
  (accumulator, currentValue) => {
    // Callback function: Adding the current value to the accumulator
    return accumulator + currentValue;
  },
  0 // Initial value of the accumulator (startingValue)
);

// Logging the result of the reduction operation to the console
console.log(result);
