// Importing the 'map' function from the '../map.js' module
let map = require("../map.js");

// An array of items to be mapped
const items = [1, 2, 3, 4, 5, 5];

// Using the 'map' function to transform each element of the 'items' array
let result = map(items, (value) => {
  // Multiply each element by 2 and return the result
  return value * 2;
});

// Logging the result of the mapping operation to the console
console.log(result);
