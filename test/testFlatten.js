// Importing the 'flatten' function from the '../flatten.js' file
const flatten = require("../flatten.js");

// A nested array to be flattened
const nestedArray = [1, [2], [[3]], [[[4]]]];

// Using the 'flatten' function to flatten the nested array
let result = flatten(nestedArray);

// Logging the result of the flattening operation to the console
console.log(result);
