// The `map` function takes an array (`elements`) and a callback function (`cb`)
function map(elements, cb) {
  // Create an empty array to store the results of the mapping operation
  let result = [];

  // Iterate over the elements array using a for loop
  for (let index = 0; index < elements.length; index++) {
    // Call the provided callback function with the current element and push the result to the 'result' array
    result.push(cb(elements[index]));
  }

  // Return the array containing the mapped values
  return result;
}

// Export the 'map' function to make it available for use in other files
module.exports = map;
