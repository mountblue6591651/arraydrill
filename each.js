// The `each` function takes an array (`elements`) and a callback function (`cb`)
function each(elements, cb) {
  // Check if the type of the 'elements' argument is 'object' (arrays are objects in JavaScript)
  if (typeof elements === "object") {
    // Iterate over the elements array using a for loop
    for (let index = 0; index < elements.length; index++) {
      // Call the provided callback function with the current element and its index
      cb(elements[index], index);
    }
  } else {
    // If 'elements' is not an array, log a message indicating that the argument is not an array
    console.log("Argument passed is not an array");
  }
}

// Export the 'each' function to make it available for use in other files
module.exports = each;
