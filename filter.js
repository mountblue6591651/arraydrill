// The `filter` function takes an array `elements` and a callback function `cb`
function filter(elements, cb) {
  // Check if the array is empty
  if (elements.length === 0) {
    // Log a message if the array is empty and return an empty array
    console.log("Array is empty");
    return [];
  } else {
    // Initialize an empty array to store the filtered elements
    let result = [];

    // Iterate over the elements array using a for loop
    for (let index = 0; index < elements.length; index++) {
      // Check if the callback returns true for the current element
      if (cb(elements[index]) === true) {
        // If true, add the element to the result array
        result.push(elements[index]);
      }
    }

    // Return the array containing the filtered elements
    return result;
  }
}

// Export the 'filter' function to make it available for use in other files
module.exports = filter;
