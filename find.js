// The `find` function takes an array `elements` and a callback function `cb`
function find(elements, cb) {
  // Check if the array is empty
  if (elements.length === 0) {
    // Log a message if the array is empty and return undefined
    console.log("Array is empty");
    return undefined;
  }

  // Iterate over the elements array using a for loop
  for (let index = 0; index < elements.length; index++) {
    // Call the provided callback function with the current element
    let arrayIncludes = cb(elements[index]);

    // Check if the callback returns true for the current element
    if (arrayIncludes == true) {
      // If true, return true as soon as a match is found
      return true;
    }
  }

  // If no match is found, return undefined
  return undefined;
}

// Export the 'find' function to make it available for use in other files
module.exports = find;
